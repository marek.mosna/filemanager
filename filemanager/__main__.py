# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

""" Prusa SLA File Manager Executable """

from pathlib import Path
from logging import DEBUG, INFO, getLogger
from logging.config import dictConfig

from gi.repository import GLib
from pydbus import SystemBus

from . import defines

from .dbus import FileManager0


def configure_log() -> None:
    """Configuring the logs"""

    level = INFO
    if Path(defines.DEBUG_FILE).exists():
        level = DEBUG
    dictConfig(
        {
            "version": 1,
            "formatters": {
                "filemanager": {"format": "%(levelname)s - %(name)s - %(message)s"}
            },
            "handlers": {
                "journald": {
                    "class": "systemd.journal.JournalHandler",
                    "formatter": "filemanager",
                    "SYSLOG_IDENTIFIER": "FILEMANAGER",
                }
            },
            "root": {"level": level, "handlers": ["journald"]},
        }
    )


def main():
    """Main"""
    configure_log()
    getLogger().info("Starting Prusa SLA File Manager")
    loop = GLib.MainLoop()
    file_manager = FileManager0()
    try:
        with SystemBus() as bus:
            with bus.publish(FileManager0.__INTERFACE__, file_manager):
                loop.run()
    except KeyboardInterrupt:
        pass
    finally:
        loop.quit()
        file_manager.stop()


if __name__ == "__main__":
    main()
