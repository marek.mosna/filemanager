# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later


from enum import unique, Enum
from typing import TYPE_CHECKING
from watchdog.events import (
    FileCreatedEvent,
    FileDeletedEvent,
    FileModifiedEvent,
    FileClosedEvent,
    DirCreatedEvent,
    DirDeletedEvent,
    DirModifiedEvent,
)

if TYPE_CHECKING:
    from watchdog.events import FileSystemEvent


@unique
class EventMonitoring(Enum):
    """Enum for monitoring Events"""

    MEDIUM_EJECTED = 1
    MEDIUM_INSERTED = 2
    FOLDER_CREATED = 3
    FOLDER_CHANGED = 4
    FOLDER_DELETED = 5
    FILE_CREATED = 6
    FILE_CHANGED = 7
    FILE_DELETED = 8
    FILE_CLOSED = 9

    @classmethod
    def to_notify(cls, event: "FileSystemEvent"):
        """Convert in EventNotify Enum"""

        return {
            FileCreatedEvent: cls.FILE_CREATED,
            FileDeletedEvent: cls.FILE_DELETED,
            FileModifiedEvent: cls.FILE_CHANGED,
            FileClosedEvent: cls.FILE_CLOSED,
            DirCreatedEvent: cls.FOLDER_CREATED,
            DirDeletedEvent: cls.FOLDER_DELETED,
            DirModifiedEvent: cls.FILE_CHANGED,
        }[event.__class__]

    def is_dir(self):
        if self in [self.FILE_CREATED, self.FILE_CHANGED, self.FILE_DELETED]:
            return False
        return True

    def is_file(self):
        return not self.is_dir()

    def to_medium(self):
        if self == self.FOLDER_DELETED:
            return self.MEDIUM_EJECTED
        if self == self.FOLDER_CREATED:
            return self.MEDIUM_INSERTED
        return self
