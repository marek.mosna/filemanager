# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any, Dict, Optional
from dataclasses import is_dataclass, asdict

from prusaerrors.shared.codes import Code
from prusaerrors.sl1.codes import Sl1Codes


def with_code(code: str):
    """
    Class decorator used to add CODE to an Exception

    :param code: Exception error code
    :return: Decorated class
    """

    def decor(cls):
        cls.CODE = code
        cls.MESSAGE = code.message
        if not isinstance(code, Code):
            raise ValueError(
                f'with_code requires valid error code string i.e "#10108", got: "{code}"'
            )
        cls.__name__ = f"e{code.raw_code}.{cls.__name__}"
        return cls

    return decor


@with_code(Sl1Codes.UNKNOWN)
class GeneralError(Exception):
    """
    General error base
    """


def wrap_exception(  # pylint: disable=invalid-name
    e: Optional[Exception],
) -> Dict[str, Any]:
    """
    Wrap exception in dictionary

    Exception is represented as dictionary str -> variant
    {
        "code": error code
        "code_specific_feature1": value1
        "code_specific_feature2": value2
        ...
    }

    :return: Exception dictionary
    """
    if not e:
        return {"code": Sl1Codes.NONE.code}

    if isinstance(e, GeneralError):
        ret = {
            "code": e.CODE.code,  # type: ignore[attr-defined]
            "name": type(e).__name__,
            "text": str(e),
        }
        if is_dataclass(e):
            ret.update(asdict(e))
        return ret

    return {
        "code": Sl1Codes.UNKNOWN.code,
        "name": type(e).__name__,
        "text": str(e),
    }


@with_code(Sl1Codes.FILE_NOT_FOUND)
class FileNotFound(GeneralError):
    """File not found Error"""


@with_code(Sl1Codes.PROJECT_ERROR_CANT_REMOVE)
class ProjectErrorCantRemove(GeneralError):
    """The current file is protected for print."""


@with_code(Sl1Codes.DIRECTORY_NOT_EMPTY)
class DirectoryNotEmpty(GeneralError):
    """The directory is not empty."""
