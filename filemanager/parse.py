# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from pathlib import Path
from string import Template
from typing import Dict, Any, List

from .sources import Source, SourceCache
from . import errors


class FSParser:
    """Default parse to walk from the directories and return a dict with all the information."""

    @staticmethod
    def get_from_path(path: Path, origin: Source, maxdepth: float) -> Dict[str, Any]:
        """
        Walk in the files/directory and return a dictionary with their descriptions.

        Args:
            path (Path): Path start point or a file
            recursive (bool, optional): If it's True walk recursively. Defaults to False.

        Raises:
            FileNotFoundError:
        """
        path.name.encode()  # let's raise to skip a file instead of failing dbus data export later

        # folder
        if path.is_dir():
            if maxdepth > 0:
                children = []
                for file_or_folder in path.glob("*"):
                    if origin.filter_path(file_or_folder):
                        try:
                            child = FSParser.get_from_path(
                                file_or_folder, origin, maxdepth - 1
                            )
                            children.append(child)
                        except UnicodeEncodeError:
                            logging.getLogger(__name__).warning(
                                'Skipping file due to a broken filename: "%s"',
                                str(file_or_folder).encode(errors="ignore").decode(),
                            )
                children.sort(key=lambda child: child["mtime"], reverse=True)
                return FSParser.process_folder(path, origin, children)
            return FSParser.process_folder(path, origin, [])
        # file
        if origin.filter_path(path):
            try:
                return FSParser.process_file(path, origin, thumbnail=True)
            except FileNotFoundError as error:
                raise errors.FileNotFound(
                    Template("File not found: $filename").substitute(filename=path)
                ) from error
        raise errors.FileNotFound(
            Template("File not found: $filename").substitute(filename=path)
        )

    @staticmethod
    def common_process(path: Path, origin: Source) -> Dict[str, Any]:
        """
        Return a dictionary with standard properties to a file or directory
        """
        return {"path": str(path), "origin": origin.value}

    @staticmethod
    def process_file(path: Path, origin: Source, **kwargs) -> Dict[str, Any]:
        """
        Return a dictionary with properties of a file
        """
        logging.getLogger(__name__).debug("Processing file %s", path)
        data = FSParser.common_process(path, origin)
        data["type"] = "file"
        try:
            stat = path.stat()
            data["size"] = stat.st_size
            data["mtime"] = max(stat.st_mtime, stat.st_ctime)
        except FileNotFoundError as error:
            raise errors.FileNotFound("File not found") from error
        if not isinstance(origin, SourceCache):
            return data
        if origin.has_cache(path):
            data["metadata"] = origin.get_cache_item(path)
        else:
            origin.process_metadata(path, data, **kwargs)
        return data

    @staticmethod
    def process_folder(
        path: Path, origin: Source, children: List[Dict[str, Any]]
    ) -> Dict[str, Any]:
        """
        Return a dictionary with properties of a directory
        """
        logging.getLogger(__name__).debug("Processing folder %s", path)
        data = FSParser.common_process(path, origin)
        try:
            stat = path.stat()
            data["mtime"] = max(stat.st_mtime, stat.st_ctime)
        except FileNotFoundError as error:
            raise errors.FileNotFound("File not found") from error
        data["type"] = "folder"
        if children:
            data["children"] = children
        return data

    @staticmethod
    def metadata_file(path: Path, origin: Source, **kwargs) -> Dict[str, Any]:
        """
        Generate the metadata from the path

        Args:
            path (Path): Path of the project
            origin (Source): Source
            thumbnail (bool, optional): Extract the thumbnails. Defaults to False.

        Returns:
            Dict[str, Any]
        """
        data = FSParser.process_file(path, origin, **kwargs)
        origin.process_metadata(path, data, **kwargs)
        return data
