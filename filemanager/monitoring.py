# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import logging
from typing import TYPE_CHECKING

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from .sources import Sources
from .events import EventMonitoring

if TYPE_CHECKING:
    from watchdog.events import FileSystemEvent


class EventHandler(FileSystemEventHandler):
    """
    Base file system event handler that you can override methods from.
    """

    def __init__(self, sources):
        self._sources = sources

    def on_moved(self, event):
        """Called when a file or a directory is moved or renamed."""
        src_event = EventMonitoring.FILE_DELETED
        dest_event = EventMonitoring.FILE_CREATED
        if event.is_directory:
            src_event = EventMonitoring.FOLDER_DELETED
            dest_event = EventMonitoring.FOLDER_CREATED

        self._sources.update_source(src_event, event.src_path)
        self._sources.update_source(dest_event, event.dest_path)

    def on_created(self, event: "FileSystemEvent"):
        """Called when a file or directory is created."""
        # When copying a file, it is not yet complete at this point
        self._sources.update_source(EventMonitoring.to_notify(event), event.src_path)

    def on_closed(self, event: "FileSystemEvent"):
        """Called when file/directory is closed(i.e. at the end of copy operation)."""
        logging.debug("CLOSED: %s", event.src_path)
        self._sources.update_source(EventMonitoring.to_notify(event), event.src_path)

    def on_deleted(self, event: "FileSystemEvent"):
        """Called when a file or directory is deleted."""
        self._sources.update_source(EventMonitoring.to_notify(event), event.src_path)


class ProjectsEvents:
    """Class for monitoring projects changes."""

    def __init__(self, sources: Sources):
        super().__init__()
        self._logger = logging.getLogger(__name__)

        self._observer = Observer()
        event_handler = EventHandler(sources)

        watch_paths = set()
        for source in sources:
            watch_path = str(source.watch_path)
            if watch_path not in watch_paths:
                watch_paths.add(watch_path)
                if not os.path.exists(watch_path):
                    os.makedirs(watch_path, exist_ok=True)
                    self._logger.debug("Creating watch path - %s", watch_path)
                self._observer.schedule(
                    event_handler, watch_path, recursive=source.watch_recursive
                )
                self._logger.debug("Starting watch - %s", watch_path)

        self._observer.start()

    def stop(self):
        """Stop monitoring"""
        self._observer.stop()
        self._observer.join()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()
