# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments

import os
import json
import zipfile
import shutil
import hashlib
import tempfile
import threading
from time import time
from pathlib import Path
from typing import Dict, Any, List
from concurrent.futures import ThreadPoolExecutor

from .. import defines
from ..events import EventMonitoring
from .local import Source
from .utils import total_size


class SourceCache(Source):
    """
    Source + cache
    """

    _executor = ThreadPoolExecutor(max_workers=2)  # type: ignore
    _max_cache_size = 10485760  # 10 MB
    __slots__ = (
        "_cache",
        "_cache_dic_lock",
        "_cache_dir_lock",
        "_cache_dir",
        "_tempdir",
    )

    def __init__(
        self,
        parent_path: Path,
        *args,
    ):
        """
        Args:
            value (str): source name
            root (str): folder path to watch
            state_changed (Signal): signal to emit when update
            parent_path (Path): parent path of temporary directory
            extensions (Tuple[str]): tuple with the files extensions supported
            extensions_deprecated (Tuple[str]): tuple with the files extensions deprecated
        """
        super().__init__(*args)

        # _cache: path => dict
        self._cache: Dict[str, Dict[str, Any]] = {}
        self._cache_dic_lock = threading.RLock()

        if not parent_path.exists():
            parent_path.mkdir()
        self._tempdir = parent_path

        self._cache_dir = parent_path / self._value
        if self._cache_dir.exists():
            shutil.rmtree(self._cache_dir)
        self._cache_dir.mkdir()

    def get_cache(self, key: Path) -> Dict[str, Any]:
        """
        Get the cache of metadata (config, thumbnail) from the source by path.

        ex.:
        if path in Source.Local:
            metadata = Source.Local[path]

        Args:
            key (Path): Path from the project file.

        Returns:
            Dict[str, Any]: Metadata dict
        """
        with self._cache_dic_lock:
            cache = self._cache.get(str(key))
            if not cache:
                cache = {}
                self._cache[str(key)] = cache
            return cache

    def has_cache(self, key: Path):
        if not key.exists():
            return False
        with self._cache_dic_lock:
            return str(key) in self._cache

    def get_cache_item(self, key: Path):
        with self._cache_dic_lock:
            return self._cache.get(str(key))

    def set_cache_item(self, key: Path, newvalue: Any):
        with self._cache_dic_lock:
            self._cache[str(key)] = newvalue

    def _clean_dir_cache(self):
        """Check the temp dir size and remove old cached files."""
        local_temp = self._cache_dir
        max_cache_size = self._max_cache_size / len(
            list(fp for fp in self._tempdir.glob("*/"))
        )
        local_size = total_size(local_temp)

        if local_size > max_cache_size:
            files = []
            with self._cache_dic_lock:
                for key, metadata in self._cache.items():
                    tempdir = metadata.get("thumbnail", {}).get("dir")
                    if tempdir:
                        tmp_path = Path(tempdir)
                        oldest = [
                            fp.stat().st_atime
                            for fp in tmp_path.glob("**/*")
                            if fp.is_file()
                        ]
                        oldest.sort()
                        files.append((oldest[0], total_size(tmp_path), key, tempdir))
                files.sort(key=lambda x: x[0], reverse=True)
                while local_size > max_cache_size:
                    tempdir_data = files.pop()
                    local_size = local_size - tempdir_data[1]
                    shutil.rmtree(tempdir_data[3])
                    del self._cache[tempdir_data[2]]["thumbnail"]

    def update_cache_children(self, event: EventMonitoring, path: Path):
        """Update cache, last_update and emit signal."""

        pathname = str(path)
        with self._cache_dic_lock:
            if event in [EventMonitoring.FILE_DELETED, EventMonitoring.FILE_CHANGED]:
                metadata = self._cache.get(pathname)
                if metadata:
                    del self._cache[pathname]
                    tempdir = metadata.get("thumbnail", {}).get("dir")
                    if tempdir:
                        shutil.rmtree(tempdir)

            if path.is_file():
                self.metadata_config(path)
                self.metadata_thumbnail(path)

            self.last_update = time()

        self._logger.info("%s %s %s", self.name, event.name, pathname)
        self._state_changed.emit(self, event, path)

    def update(self, event: EventMonitoring, path: Path) -> bool:
        return self.update_cache(event, path)

    def update_cache(self, event: EventMonitoring, path: Path) -> bool:
        """
        Update the metadata cache with the event and path

        Args:
            event (Event): one of [CREATE, DELETE, MODIFY]
            path (Path): Path from folder/file changed

        Returns:
            bool: True if the path belongs to the source.
        """

        if event.is_file():
            if path.suffix not in self._extensions:
                return False

        self._executor.submit(self.update_cache_children, event, path)
        return True

    def metadata_config(self, path: Path) -> Dict[str, Any]:
        """Return all metadata from the project

        Args:
            path (Path): File Path

        Returns:
            Dict[str, Any]
        """

        metadata = self.get_cache(path)

        if "config" not in metadata:
            try:
                data: Dict[str, Any] = {}
                with self._cache_dic_lock:
                    with zipfile.ZipFile(path, "r") as zip_file:
                        config_file = zip_file.read(defines.PROJECT_CONFIG_FILE).decode(
                            "utf-8"
                        )
                        for line in [
                            l
                            for l in (line.strip() for line in config_file.splitlines())
                            if l
                        ]:
                            try:
                                (key, value) = line.split(" = ")
                                data[key] = json.loads(value)
                            except json.decoder.JSONDecodeError:
                                data[key] = value
                        metadata["config"] = data
            except zipfile.BadZipFile:
                metadata["config"] = {"error": "Corrupted"}

        return metadata["config"]

    def metadata_thumbnail(self, path: Path) -> Dict[str, Any]:
        """Return all images preview from the project

        Args:
            path (Path): File Path

        Returns:
            Dict[str, Any]
        """

        metadata = self.get_cache(path)
        if "thumbnail" not in metadata:
            saved_files: List[str] = []
            data: Dict[str, Any] = {"files": saved_files}
            temdir = None
            try:
                with self._cache_dic_lock:
                    with zipfile.ZipFile(path, "r") as zip_file:
                        for info_file in zip_file.infolist():
                            if info_file.filename.startswith("thumbnail/"):
                                if not temdir:
                                    temdir = tempfile.mkdtemp(dir=self._cache_dir)
                                    os.chmod(temdir, defines.PROJECT_DIR_CACHE)
                                    data["dir"] = temdir
                                filename = zip_file.extract(
                                    info_file.filename, path=temdir
                                )
                                if os.path.isfile(filename):
                                    saved_files.append(filename)
                metadata["thumbnail"] = data
            except zipfile.BadZipFile:
                if temdir and os.path.isdir(temdir):
                    shutil.rmtree(temdir)
                metadata["thumbnail"] = {"error": "Corrupted"}

        return metadata["thumbnail"]

    def process_metadata(self, path: Path, data: Dict[str, Any], **kwargs):
        """include metadata in data dict"""
        thumbnail = kwargs.get("thumbnail", False)
        metadata = self.get_cache(path)
        if "config" not in metadata:
            metadata["config"] = self.metadata_config(path)
        if thumbnail and "thumbnail" not in metadata:
            metadata["thumbnail"] = self.metadata_thumbnail(path)
        data["metadata"] = metadata

    @property
    def etag(self) -> str:
        """
        Return the identification of the change of the data or in the cache.
        """
        with self._cache_dic_lock:
            etag = hashlib.sha1()  # nosec
            etag.update(str(self.last_update).encode("utf-8"))
            for st_ino in self._cache:
                etag.update(str(st_ino).encode("utf-8"))
            return f'W/"{etag.hexdigest()}"'
