# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess  # nosec
from pathlib import Path
from typing import Optional, Dict, Union

from .. import events
from .local import Source


class UsbSource(Source):
    """USB source"""

    def __init__(self, *args):
        super().__init__(*args)
        self._watch_recursive = False

    @property
    def root(self) -> Optional[Path]:
        """
        Return the root path or None if doesn't exist
        """
        path = None
        usb_list = [p for p in self._watch_path.glob("*") if p.is_mount()]
        if usb_list:
            path = usb_list[0]
        return path

    @property
    def disk_usage(self) -> Dict[str, float]:
        """Return disk usage statistics about the given path as a dictionary with the attributes
        total, used and free, which are the amount of total, used and free space, in bytes.
        """
        if self.root:
            return super().disk_usage

        return {}

    @staticmethod
    def ch_mode_owner(path: Union[str, Path]) -> None:
        """
        change group and mode of the file or folder.
        """

    def remove(self, path: Path):
        """remove file"""
        subprocess.check_call(["usbremount", str(path)])  # nosec
        path.unlink()
        self.update(events.EventMonitoring.FILE_DELETED, path)

    def remove_dir(self, path: Path, recursive: bool = False):
        """remove dir"""
        subprocess.check_call(["usbremount", str(path)])  # nosec
        super().remove_dir(path, recursive)
        self.update(events.EventMonitoring.FOLDER_DELETED, path)

    def rename(self, src: str, dst: str):
        """rename file"""
        subprocess.check_call(["usbremount", str(src)])  # nosec
        is_dir = os.path.isdir(src)
        os.rename(src, dst)
        event_from = events.EventMonitoring.FILE_DELETED
        event_to = events.EventMonitoring.FILE_CREATED
        if is_dir:
            event_from = events.EventMonitoring.FOLDER_DELETED
            event_to = events.EventMonitoring.FOLDER_CREATED
        self.update(event_from, Path(src))
        self.update(event_to, Path(dst))

    def remount(self, src: str):
        """Remount path to execute an action

        Args:
            src (str): path for remount
        """
        subprocess.check_call(["usbremount", str(src)])  # nosec

    def update(self, event: events.EventMonitoring, path: Path) -> bool:
        """
        Update the metadata cache with the event and path

        Args:
            event (Event): one of [CREATE, DELETE, MODIFY]
            path (Path): Path from folder/file changed

        Returns:
            bool: True if the path belongs to the source.
        """

        to_notify = event
        if path.parent == self._watch_path:
            to_notify = event.to_medium()

        return super().update(to_notify, path)
