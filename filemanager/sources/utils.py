# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import fcntl
import array
import shutil
from pathlib import Path
from typing import Union

from .. import defines

EXT2_IOC_GETFLAGS = 0x80086601
EXT2_IOC_SETFLAGS = 0x40086602
EXT2_IMMUTABLE_FL = 0x00000010


def total_size(path: Path):
    """Calculate the size of the path."""
    return sum(fp.stat().st_size for fp in path.glob("**/*") if fp.is_file())


def ch_mode_owner(path: Union[str, Path]):
    """
    change group and mode of the file or folder.
    """
    shutil.chown(path, group=defines.PROJECT_GROUP)
    if os.path.isdir(path):
        os.chmod(path, defines.PROJECT_DIR_MODE)
        for name in os.listdir(path):
            ch_mode_owner(os.path.join(path, name))
    else:
        os.chmod(path, defines.PROJECT_MODE)


def make_mutable(filename: Union[str, Path]):
    """
    Turn the file mutable.
    """
    flags = array.array("l", [0])
    file_descriptor = os.open(filename, os.O_RDONLY)
    try:
        fcntl.ioctl(file_descriptor, EXT2_IOC_GETFLAGS, flags, True)
        flags[0] &= ~EXT2_IMMUTABLE_FL
        fcntl.ioctl(file_descriptor, EXT2_IOC_SETFLAGS, flags)
    finally:
        os.close(file_descriptor)


def make_immutable(filename: Union[str, Path]) -> None:
    """
    A file cannot be modified: it cannot be deleted or renamed,
    no link can be created to this file and no data can be written to the file.
    """
    flags = array.array("l", [0])
    file_descriptor = os.open(filename, os.O_RDWR)
    try:
        fcntl.ioctl(file_descriptor, EXT2_IOC_GETFLAGS, flags, True)
        flags[0] |= EXT2_IMMUTABLE_FL
        fcntl.ioctl(file_descriptor, EXT2_IOC_SETFLAGS, flags)
    finally:
        os.close(file_descriptor)
