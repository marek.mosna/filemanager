# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=no-self-use


import os
import errno
import shutil
import logging
import threading
from time import time
from pathlib import Path
from typing import Optional, Dict, Any, Tuple, Union, Generator

from PySignal import Signal

from .. import defines
from .. import errors, events

from .utils import ch_mode_owner as cmo


class Source:
    """
    This class is an abstraction of the source files tree.
    Contain information about the source files and maintain a cache of project metadata.
    """

    __slots__ = (
        "_logger",
        "_value",
        "_watch_path",
        "_watch_recursive",
        "_state_changed",
        "_last_update_lock",
        "_last_update",
        "_extensions",
        "_extensions_deprecated",
    )

    def __init__(
        self,
        value: str,
        root: str,
        state_changed: Signal,
        extensions: Tuple[str],
        extensions_deprecated: Tuple[str, ...],
    ):
        """
        Args:
            value (str): source name
            root (str): folder path to watch
            state_changed (Signal): signal to emit when update
            extensions (Tuple[str]): tuple with the files extensions supported
            extensions_deprecated (Tuple[str]): tuple with the files extensions deprecated
        """

        self._logger = logging.getLogger()
        self._value = value
        self._watch_path = Path(root)
        self._watch_recursive = True
        self._state_changed = state_changed
        self._extensions = extensions
        self._extensions_deprecated = extensions_deprecated
        self._last_update_lock = threading.Lock()
        self._last_update = time()

    @property
    def disk_usage(self) -> Dict[str, float]:
        """Return disk usage statistics about the given path as a dictionary with the attributes
        total, used and free, which are the amount of total, used and free space, in bytes.
        """
        return shutil.disk_usage(self._watch_path)._asdict()

    @property
    def name(self) -> str:
        """
        Source name (Similar Enum)

        ex.:
        Sources.LOCAL.name == "LOCAL"
        """
        return self._value.upper()

    @property
    def value(self) -> str:
        """
        Source value

        ex.:
        Sources.LOCAL.value == "local"
        """
        return self._value

    @property
    def last_update(self) -> float:
        """
        Last update timestamp of the source
        """
        with self._last_update_lock:
            return self._last_update

    @last_update.setter
    def last_update(self, value):
        with self._last_update_lock:
            self._last_update = value

    @property
    def root(self) -> Optional[Path]:
        """
        Return the root path or None if doesn't exist
        """
        return self._watch_path

    @property
    def watch_path(self):
        """
        Return the parent path to be watched
        """
        return self._watch_path

    @property
    def watch_recursive(self):
        return self._watch_recursive

    def is_parent(self, path: Path) -> bool:
        """It checks if the path belongs to the root.

        Args:
            path (Path)

        Returns:
            bool
        """
        if path.as_posix().startswith(self._watch_path.as_posix()):
            return True
        return False

    def filter_path(self, path: Path) -> bool:
        """
        Return True if the path is a valid else False
        """

        # remove hidding folders/files
        for element in path.parts:
            if element.startswith("."):
                return False

        # remove not projects files
        if path.is_file():
            if path.suffix in self._extensions:
                return True
        else:
            for _, _, files in os.walk(path):
                for file in files:
                    (_, ext) = os.path.splitext(file)
                    if ext in self._extensions:
                        return True
        return False

    def __repr__(self):
        return f"<Sources.{self._value}: root={self._watch_path}>"

    def __str__(self):
        return f"Sources.{self._value}"

    def process_metadata(self, path: Path, data: Dict[str, Any], **kwargs):
        """include metadata in data dict"""

    @staticmethod
    def ch_mode_owner(path: Union[str, Path]) -> None:
        """
        change group and mode of the file or folder.
        """
        cmo(path)

    def remove(self, path: Path):
        """remove file"""
        path.unlink()

    def remove_dir(
        self, path: Path, recursive: bool = False
    ):  # pylint: disable = no-self-use
        """remove dir"""
        if path == self.watch_path:
            raise errors.ProjectErrorCantRemove()
        try:
            if recursive:
                shutil.rmtree(path)
            else:
                os.rmdir(path)
        except OSError as error:
            if error.errno == errno.ENOTEMPTY:
                raise errors.DirectoryNotEmpty()
            raise

    def rename(self, src: str, dst: str):
        """rename file"""
        os.rename(src, dst)
        cmo(dst)

    def remount(self, src: str):
        """Remount path to execute an action

        Args:
            src (str): path for remount
        """

    def deprecation_detector(self) -> Generator[str, None, None]:
        if self.root is None:
            return
        for root, _, files in os.walk(self.root):
            for name in files:
                (_, ext) = os.path.splitext(name)
                if ext in self._extensions_deprecated:
                    yield os.path.join(root, name)

    @property
    def has_deprecated(self) -> bool:
        for _ in self.deprecation_detector():
            return True
        return False

    def move_deprecated(self) -> None:
        """Move files to defines.OLD_PROJECTS_DOWNLOAD,
        It will rename the file relative the root path.
        """
        for src in self.deprecation_detector():
            file_name = os.path.relpath(src, start=self.root).replace("/", "_")
            dst = os.path.join(defines.OLD_PROJECTS_DOWNLOAD, file_name)
            count = 1
            while os.path.exists(dst):
                (dst_path, ext) = os.path.splitext(dst)
                dst = f"{dst_path}_copy{count}{ext}"
            os.link(src, dst)
            self.remove(Path(src))

    def remove_deprecated(self) -> None:
        for path in self.deprecation_detector():
            self.remove(Path(path))

    @property
    def extensions(self) -> Tuple[str]:
        return self._extensions

    @property
    def extensions_deprecated(self) -> Tuple[str, ...]:
        return self._extensions_deprecated

    def update(self, event: events.EventMonitoring, path: Path) -> bool:
        """
        Update the metadata cache with the event and path

        Args:
            event (Event): one of [CREATE, DELETE, MODIFY]
            path (Path): Path from folder/file changed

        Returns:
            bool: True if the path belongs to the source.
        """

        if event.is_file():  # if the file is deleted path.is_file() will be False
            if path.suffix not in self._extensions:
                return False

        self.last_update = time()
        self._logger.info("%s %s %s", self.name, event.name, str(path))
        self._state_changed.emit(self, event, path)
        return True
