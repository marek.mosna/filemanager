# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments

import os
import gc
import shutil
from logging import getLogger
import tempfile
import configparser
from time import time_ns
from pathlib import Path
from string import Template
from datetime import timedelta
from typing import Dict, Any, Tuple, List, Optional, Type

from PySignal import Signal

from .. import defines
from .. import errors
from ..events import EventMonitoring
from ..model import PrinterModel
from ..parse import FSParser

from .local import Source
from .local_cache import SourceCache
from .usb import UsbSource
from .usb_cache import UsbSourceCache


class Local(SourceCache):
    """LOCAL source"""

    def __init__(
        self, printer_model: PrinterModel, state_changed: Signal, tempdir: Path
    ):
        extensions = printer_model.extensions
        extensions_deprecated = printer_model.extensions_deprecated
        super().__init__(
            tempdir,
            "local",
            defines.PROJECT_ROOT_PATH,
            state_changed,
            extensions,
            extensions_deprecated,
        )


class Usb(UsbSourceCache):
    """USB source"""

    def __init__(
        self,
        printer_model: PrinterModel,
        state_changed: Signal,
        tempdir: Path,
        current_media_path_changed: Signal = Signal(),
    ):
        extensions = printer_model.extensions
        extensions_deprecated = printer_model.extensions_deprecated
        state_changed.connect(self.filter_media_change)
        self.current_media_path_changed = current_media_path_changed
        super().__init__(
            tempdir,
            "usb",
            defines.PROJECT_MEDIA_PATH,
            state_changed,
            extensions,
            extensions_deprecated,
        )

    def filter_media_change(self, source: Source, event: EventMonitoring, path: Path):
        """If the current medium has changed, propagate this change up."""

        if self != source:
            self._logger.warning("event not from Usb")
            return

        def is_media_root(_path: Path):
            return (
                len(str(_path).split("/"))
                == len(str(defines.PROJECT_MEDIA_PATH).split("/")) + 1
            )

        if event == EventMonitoring.MEDIUM_INSERTED:
            if path.is_dir() and is_media_root(path):
                self.current_media_path_changed.emit(path)
        if event == EventMonitoring.MEDIUM_EJECTED:
            self.current_media_path_changed.emit("")
            self._executor.submit(self._clean_dir_cache)


class PreviousPrintsSource(SourceCache):
    """Previous source"""

    def __init__(
        self, printer_model: PrinterModel, state_changed: Signal, tempdir: Path
    ):
        extensions = printer_model.extensions
        extensions_deprecated = printer_model.extensions_deprecated
        super().__init__(
            tempdir,
            "previous-prints",
            defines.PROJECT_PREVIOUS_PATH,
            state_changed,
            extensions,
            extensions_deprecated,
        )

    def metadata_config(self, path: Path) -> Dict[str, Any]:
        """Return all metadata from the project, including metadata
        saved in /var/sl1fw/previous-prints/info.ini as configparser.ConfigParser()
        """
        metadata = self.get_cache(path)
        if "config" not in metadata:
            metadata = super().metadata_config(path)
            info_path = (
                Path(defines.PROJECT_PREVIOUS_PATH) / defines.PROJECT_PREVIOUS_INFO
            )
            if info_path.is_file():
                config = configparser.ConfigParser()
                config.read(info_path)
                default = config["metadata"]
                for key in default:
                    metadata[key] = default[key]
        return metadata


class UsbUpdates(UsbSource):
    """USB raucb updates source"""

    def __init__(
        self,
        state_changed: Signal,
    ):
        super().__init__(
            "raucb",
            defines.UPDATE_MEDIA_PATH,
            state_changed,
            (".raucb",),
            (),
        )


# pylint: disable = invalid-name
class Sources:
    """Map all sources"""

    __instance = None
    _tempdir = Path(tempfile.gettempdir()) / "projects_metadata"
    __slots__ = (
        "state_changed",
        "current_media_path_changed",
        "extension",
        "LOCAL",
        "USB",
        "PREVIOUS_PRINTS",
        "USB_UPDATES",
        "_sources",
        "_i",
        "_max",
        "_logger",
    )

    def __init__(self):

        if Sources.__instance is not None:
            raise Exception("This class is a singleton!")

        self._logger = getLogger(__name__)
        self.state_changed = Signal()
        self.current_media_path_changed = Signal()
        printer_model = PrinterModel.get_model()
        self.extension = printer_model.extensions[0]
        self.LOCAL = Local(printer_model, self.state_changed, self._tempdir)
        self.USB = Usb(
            printer_model,
            state_changed=self.state_changed,
            tempdir=self._tempdir,
            current_media_path_changed=self.current_media_path_changed,
        )
        self.PREVIOUS_PRINTS = PreviousPrintsSource(
            printer_model, self.state_changed, self._tempdir
        )
        self.USB_UPDATES = UsbUpdates(self.state_changed)
        self._sources = [self.LOCAL, self.USB, self.PREVIOUS_PRINTS, self.USB_UPDATES]
        self._max = len(self._sources)
        self._i = 0

    def __iter__(self):
        self._i = 0
        return self

    def __next__(self) -> Source:
        i = self._i
        if i < self._max:
            self._i += 1
            return self._sources[i]

        raise StopIteration

    def update_source(self, event: EventMonitoring, pathname: str) -> None:
        """
        Find and update the metadata cache with the event and path

        Args:
            event (Event): one of [CREATE, DELETE, MODIFY]
            pathname (str): Path from folder/file changed
        """

        path = Path(pathname)

        for element in path.parts:
            if element.startswith("."):
                return

        for source in self._sources:
            if source.is_parent(path):
                if source.update(event, path):
                    self._logger.debug(
                        "Receive event %s to %s for path %s",
                        event.name,
                        source.name,
                        path,
                    )

    def path_to_source(self, external_path: str) -> Tuple[Source, Path]:
        """Return the Source and Path

        Args:
            external_path (str)

        Raises:
            FileNotFoundError: Throw when the path doesn't belong to any source.

        Returns:
            Tuple[Source, Path]: (source, path)
        """
        path = Path(external_path).resolve()
        for source in self._sources:
            if source.is_parent(path):
                return (source, path)

        raise errors.FileNotFound(
            Template("File not found: $filename").substitute(filename=external_path)
        )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self._tempdir.is_dir():
            shutil.rmtree(self._tempdir)

    def move(self, src: str, dst: str) -> None:  # pylint: disable=too-many-branches
        """
        move project
        """
        try:
            from_origin = None
            to_origin = None
            from_path = Path(src).resolve()
            to_path = Path(dst).resolve()
            is_dir = from_path.is_dir()

            for source in self._sources:
                if source.is_parent(from_path):
                    from_origin = source
                if source.is_parent(to_path):
                    to_origin = source

            if from_origin == to_origin:
                try:
                    if from_origin:
                        from_origin.rename(from_path, to_path)
                    else:
                        os.rename(from_path, to_path)
                    return
                except OSError:
                    pass

            # before move
            if to_origin:
                to_origin.remount(to_path)

            shutil.copyfile(from_path, to_path)

            # after move
            if from_origin:
                from_origin.remove(from_path)
            else:
                os.unlink(from_path)

            if to_origin and not to_origin.watch_recursive:
                event_to = EventMonitoring.FILE_CREATED
                if is_dir:
                    event_to = EventMonitoring.FOLDER_CREATED
                to_origin.ch_mode_owner(to_path)
                self.update_source(event_to, str(to_path))
        except (FileNotFoundError, IsADirectoryError) as error:
            raise errors.FileNotFound(
                Template("File not found: $filename").substitute(
                    filename=error.filename
                )
            ) from error

    def current_project_set(self, path: str) -> None:
        """Point PROJECT_SYMLINK to path
            The file at this path won't be possible to remove without call clean_protection first.

        Args:
            path (str): Path of the project to print.
        """
        try:
            if not defines.PROJECT_SYMLINK.exists():
                (origin, internal_path) = self.path_to_source(path)
                defines.PROJECT_SYMLINK.symlink_to(internal_path)
                origin.ch_mode_owner(internal_path)
            else:
                raise errors.ProjectErrorCantRemove()
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error

    @staticmethod
    def current_project_get() -> str:
        return (
            str(defines.PROJECT_SYMLINK.resolve())
            if defines.PROJECT_SYMLINK.exists()
            else ""
        )

    @staticmethod
    def current_project_clean() -> None:
        if defines.PROJECT_SYMLINK.exists():
            defines.PROJECT_SYMLINK.unlink()

    def average_extraction_time(self, path: str) -> List[str]:
        # pylint: disable=too-many-locals
        (origin, internal_path) = self.path_to_source(path)
        source: Optional[Type[SourceCache]] = None
        if origin is self.USB:
            source = Usb
        elif origin is self.LOCAL:
            source = Local
        else:
            raise ValueError("Only supported sources local and usb.")

        N = 1000
        mtime = 0
        signal = Signal()
        printer_model = PrinterModel.get_model()
        t_init = time_ns()
        gcold = gc.isenabled()
        gc.disable()
        try:
            for i in range(0, N):
                with tempfile.TemporaryDirectory() as tmpdirname:
                    self._logger.debug("average_extraction_time: %.2f %%", i / N)
                    s = source(printer_model, signal, Path(tmpdirname))
                    t0 = time_ns()  # Start
                    FSParser.metadata_file(
                        internal_path,
                        s,
                        thumbnail=True,
                    )  # type: ignore
                    t1 = time_ns()  # End
                    mtime += t1 - t0
        finally:
            if gcold:
                gc.enable()

        total = timedelta(
            microseconds=(time_ns() - t_init) / 1000,
        )
        average = timedelta(
            microseconds=(mtime / N) / 1000,
        )

        return [
            f"Total running testing: {total}",
            f"Average extraction time: {average}",
        ]
