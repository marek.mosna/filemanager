# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=too-many-function-args
# pylint: disable=super-init-not-called

import shutil
from logging import getLogger
from time import time
from pathlib import Path

from ..events import EventMonitoring
from .usb import UsbSource
from .local_cache import SourceCache


class UsbSourceCache(UsbSource, SourceCache):
    """USB source + cache"""

    def __init__(
        self,
        *args,
    ):
        """
        Args:
            value (str): source name
            root (str): folder path to watch
            state_changed (Signal): signal to emit when update
            parent_path (Path): parent path of temporary directory
            extensions (Tuple[str]): tuple with the files extensions supported
            extensions_deprecated (Tuple[str]): tuple with the files extensions deprecated
        """
        SourceCache.__init__(self, *args)
        self._watch_recursive = False

    def update_cache_root(self, event: EventMonitoring, path: Path):
        """Update cache, last_update and emit signal."""

        pathname = str(path)
        to_notify = event
        with self._cache_dic_lock:
            if event == EventMonitoring.FOLDER_DELETED:
                with self._cache_dic_lock:
                    self._cache = {}

                    shutil.rmtree(self._cache_dir)
                    self._cache_dir.mkdir()
                    to_notify = EventMonitoring.MEDIUM_EJECTED

            if event == EventMonitoring.FOLDER_CREATED:
                ext = "|".join(self.extensions)
                projects = path.glob(f"**/*[{ext}]")
                newest_files = sorted(
                    projects,
                    key=lambda proj: max(proj.stat().st_mtime, proj.stat().st_ctime),
                )
                if newest_files and self.filter_path(newest_files[-1]):
                    newest_file = newest_files[-1]
                    self.metadata_config(newest_file)
                    self.metadata_thumbnail(newest_file)
                    path = newest_file

                to_notify = EventMonitoring.MEDIUM_INSERTED

        self.last_update = time()
        getLogger().info("%s %s %s", self.name, to_notify.name, pathname)
        self._state_changed.emit(self, to_notify, path)

    def update(self, event: EventMonitoring, path: Path) -> bool:
        """
        Update the metadata cache with the event and path

        Args:
            event (Event): one of [CREATE, DELETE, MODIFY]
            pathname (str): Path from folder/file changed

        Returns:
            bool: True if the path belongs to the source.
        """

        if path.parent == self._watch_path:
            self._executor.submit(self.update_cache_root, event, path)
            return True

        return super().update_cache(event, path)
