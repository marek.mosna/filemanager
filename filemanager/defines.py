# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import stat
from pathlib import Path

PROJECT_ROOT_PATH = "/var/sl1fw/projects"
PROJECT_MEDIA_PATH = "/run/media/system"
PROJECT_PREVIOUS_PATH = "/var/sl1fw/previous-prints/"
PROJECT_PREVIOUS_INFO = "info.ini"
PROJECT_GROUP = "projects"
PROJECT_MODE = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH
PROJECT_DIR_MODE = stat.S_IRWXU | stat.S_IRWXG | stat.S_IROTH
PROJECT_DIR_CACHE = (
    stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH
)
UPDATE_MEDIA_PATH = PROJECT_MEDIA_PATH
PRINTER_MODEL_PATH = "/run/model"
OLD_PROJECTS_DOWNLOAD = "/var/sl1fw/old-projects"
PROJECT_CONFIG_FILE = "config.ini"

PROJECT_SYMLINK = Path("/var/sl1fw/current_print_project")

DEBUG_FILE = "/etc/debug"
