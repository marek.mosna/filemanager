# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from enum import Enum, unique

from . import defines


@unique
class PrinterModel(Enum):
    """
    Printer model
    """

    NONE = 0
    SL1 = 1
    SL1S = 2
    M1 = 3

    @property
    def extensions(self):
        return {
            self.NONE: ("",),
            self.SL1: (".sl1",),
            self.SL1S: (".sl1s",),
            self.M1: (".m1",),
        }[self]

    @property
    def extensions_deprecated(self):
        return {
            self.NONE: ("",),
            self.SL1: (
                ".dwz",
                ".sl1s",
                ".m1",
            ),
            self.SL1S: (
                ".dwz",
                ".sl1",
                ".m1",
            ),
            self.M1: (
                ".dwz",
                ".sl1",
                ".sl1s",
            ),
        }[self]

    @classmethod
    def get_model(cls):
        for model in cls:
            path = os.path.join(defines.PRINTER_MODEL_PATH, model.name.lower())
            if os.path.isfile(path):
                return model
        return cls.NONE
