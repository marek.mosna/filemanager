# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=C0301
# pylint: disable=missing-function-docstring
# pylint: disable=no-self-use

from __future__ import annotations
import os
import hashlib
from math import inf
from pathlib import Path
from typing import Any, Dict, Optional, List

from pydbus.generic import signal

from slafw.api.decorators import (  # pylint: disable=import-error
    auto_dbus,
    dbus_api,
    last_error,
    wrap_dict_data,
    auto_dbus_signal,
)

from .monitoring import ProjectsEvents
from .sources import Source, SourceCache, Sources
from .parse import FSParser
from . import errors
from .events import EventMonitoring
from . import defines


@dbus_api
class FileManager0:
    """Dbus api for share file system data"""

    __INTERFACE__ = "cz.prusa3d.sl1.filemanager0"

    PropertiesChanged = signal()

    @auto_dbus_signal
    def MediaInserted(self, path: str):  # pylint: disable=invalid-name
        pass

    @auto_dbus_signal
    def MediaEjected(self, root_path: str):  # pylint: disable=invalid-name
        pass

    def __init__(self):
        self._last_exception: Optional[Exception] = None
        self._sources = Sources()
        self._sources.state_changed.connect(self._files_update)
        last_update = {}
        for source in self._sources:
            last_update[source.value] = source.last_update
        self._last_update = last_update
        self._projects_events = ProjectsEvents(self._sources)
        self._current_media_path = ""
        self._sources.current_media_path_changed.connect(
            self._current_media_path_update
        )

        # Check if there is a medium inserted already
        self._current_media_path_update()

    def _current_media_path_update(self, path: Path = Path("")):
        if path == Path(""):
            # Set path is empty, check if there is another medium mounted
            self._current_media_path = ""
            for item in os.scandir(defines.PROJECT_MEDIA_PATH):
                if item.is_dir():
                    self._current_media_path = item.path
                    self.PropertiesChanged(
                        self.__INTERFACE__,
                        {"current_media_path": str(self._current_media_path)},
                        [],
                    )
        elif path != self._current_media_path:
            self._current_media_path = path
            self.PropertiesChanged(
                self.__INTERFACE__,
                {"current_media_path": str(self._current_media_path)},
                [],
            )

    def _files_update(self, source: Source, event: EventMonitoring, path: Path):

        # last_update
        self._last_update[source.value] = source.last_update
        self.PropertiesChanged(
            self.__INTERFACE__, {"last_update": wrap_dict_data(self._last_update)}, []
        )

        # media
        if source is self._sources.USB:
            if event == EventMonitoring.MEDIUM_INSERTED:
                newest_project = ""
                if path.exists():
                    newest_project = str(path)
                self.MediaInserted(newest_project)
            elif event == EventMonitoring.MEDIUM_EJECTED:
                self.MediaEjected(str(path))

    def stop(self):
        """Stop Dbus"""
        self._projects_events.stop()

    @auto_dbus  # type: ignore
    @property
    def current_media_path(self) -> str:
        """str: Return the path to the latest media/usb flashdisk mounted. Empty string otherwise."""

        return str(self._current_media_path)

    @auto_dbus  # type: ignore
    @property
    def last_update(self) -> Dict[str, Any]:
        """float: Return the time in seconds for the last time the project files were updated."""

        return wrap_dict_data(self._last_update)

    @auto_dbus  # type: ignore
    @property
    def last_exception(self) -> Dict[str, Any]:
        """Last Exception data

        Returns:
            Dict[str, Any]
        """
        return wrap_dict_data(errors.wrap_exception(self._last_exception))

    @auto_dbus  # type: ignore
    @last_error
    def get_all(self, maxdepth: int) -> Dict[str, Any]:
        """Return a dict with info all files

        sample:
        {
            "local": {
                "path": "/var/sl1fw/projects",
                "origin": "local",
                "type": "folder",
                "children": [
                {
                    "path": "/var/sl1fw/projects/examples",
                    "origin": "local",
                    "type": "folder",
                    "children": [
                    {
                        "path": "/var/sl1fw/projects/examples/Petrin_Tower_10H_50um_Prusa_Orange.sl1",
                        "origin": "local",
                        "type": "file",
                        "size": 21008282
                    },
                    {
                        "path": "/var/sl1fw/projects/examples/Calibration objects",
                        "origin": "local",
                        "type": "folder",
                        "children": [
                        {
                            "path": "/var/sl1fw/projects/examples/Calibration objects/Resin_Calibration_Object_0.025.sl1",
                            "origin": "local",
                            "type": "file",
                            "size": 4930590
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            "last_update": {
                "last_update": 1599835353,
                "local": 1599835353
            }
        }

        Returns:
            Dict[str, Any]
        """
        max_depth = inf if maxdepth < 0 else maxdepth
        result = {}
        for source in self._sources:
            root_path = source.root
            if root_path:
                sources_tree = FSParser.get_from_path(root_path, source, max_depth)
                result[source.value] = sources_tree
        result["last_update"] = self._last_update
        return wrap_dict_data(result)

    @auto_dbus  # type: ignore
    @last_error
    def get_from_path(self, path: str, maxdepth: int) -> Dict[str, Any]:
        """Return a dict with info about one specific file or folder.

        sample:
        {
            "files": {
                "path": "/var/sl1fw/projects/examples/Petrin_Tower_10H_50um_Prusa_Orange.sl1",
                "type": "file",
                "size": 21008282
            },
            "last_update": 1599112560
        }

        Args:
            path (str): path to the folder or file
            recursive (bool): When is a folder return recursively, the file will ignore it, but it has to be sent because of the API dbus architecture.

        Returns:
            Dict[str, Any]
        """
        max_depth = inf if maxdepth < 0 else maxdepth
        (origin, internal_path) = self._sources.path_to_source(path)
        return wrap_dict_data(
            {
                "files": FSParser.get_from_path(internal_path, origin, max_depth),
                "last_update": self._last_update,
            }
        )

    @auto_dbus  # type: ignore
    @last_error
    def get_metadata(self, path: str, thumbnail: bool) -> Dict[str, Any]:
        """Extract all metadata from a project file and, optionally, the preview images.
           The user will be responsible for removing the temporary folder afterward.

        sample:
        {
            "files": {
                "path": "/var/sl1fw/projects/examples/Calibration objects/Resin_Calibration_Object_0.050.sl1",
                "type": "file",
                "size": 4304037,
                "metadata": {
                    "action": "print",
                    "jobDir": "Resin calibration object",
                    "expTime": 5,
                    "expTimeFirst": 35,
                    "fileCreationTimestamp": "2019-12-11 at 09:25:00 UTC",
                    "layerHeight": 0.05,
                    "materialName": "Prusa Orange Tough 0.05",
                    "numFade": 10,
                    "numFast": 502,
                    "numSlow": 0,
                    "printProfile": "0.05 Normal",
                    "printTime": 5257.272727,
                    "printerModel": "SL1",
                    "printerProfile": "Original Prusa SL1",
                    "printerVariant": "default",
                    "prusaSlicerVersion": "PrusaSlicer-2.1.0+win64-201909160915",
                    "usedMaterial": 2.135513,
                    "calibrateRegions": 8,
                    "calibrateTime": 1,
                    "calibratePadThickness": 0.5,
                    "calibrateTextThickness": 0.5
                },
                "preview": {
                    "dir": "/tmp/tmpy36n51rt",
                    "images": [
                        "/tmp/tmpy36n51rt/thumbnail/thumbnail400x400.png",
                        "/tmp/tmpy36n51rt/thumbnail/thumbnail800x480.png"
                    ]
                }
            },
            "last_update": 1599112560
        }

        Args:
            path (str): path to the folder or file
            preview (bool): create a temporary folder and unzip all preview files over there

        Returns:
            Dict[str, Any]
        """
        (origin, internal_path) = self._sources.path_to_source(path)
        return wrap_dict_data(
            {
                "files": FSParser.metadata_file(
                    internal_path, origin, thumbnail=thumbnail
                ),
                "last_update": self._last_update,
            }
        )

    @auto_dbus
    @last_error
    def remove(self, path: str) -> None:
        """
        remove project
        """
        try:
            (origin, internal_path) = self._sources.path_to_source(path)
            origin.remove(internal_path)
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error

    @auto_dbus
    @last_error
    def remove_dir(self, path: str, recursive: bool) -> None:
        """
        remove project
        """
        try:
            (origin, internal_path) = self._sources.path_to_source(path)
            origin.remove_dir(internal_path, recursive)
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error

    @auto_dbus
    def move(self, src: str, dst: str) -> None:
        """
        move project
        """
        self._sources.move(src, dst)

    @auto_dbus
    @last_error
    def current_project_set(self, path: str) -> None:
        """Point PROJECT_SYMLINK to path
            The file at this path won't be possible to remove without calling clean_protection first.

        Args:
            path (str): Path of the project to print.
        """
        self._sources.current_project_set(path)

    @auto_dbus
    @last_error
    def current_project_get(self) -> str:
        """Path PROJECT_SYMLINK points to"""
        return self._sources.current_project_get()

    @auto_dbus
    @last_error
    def current_project_clean(self) -> None:
        """Remove PROJECT_SYMLINK"""
        self._sources.current_project_clean()

    @auto_dbus  # type: ignore
    @property
    def disk_usage(self) -> Dict[str, Any]:
        """Return disk usage statistics about the given path as a dictionary with the attributes
        total, used and free, which are the amount of total, used and free space, in bytes.
        """
        result = {}
        for source in self._sources:
            disk_usage = source.disk_usage
            if disk_usage:
                result[source.value] = disk_usage
        return wrap_dict_data(result)

    @auto_dbus  # type: ignore
    @property
    def etag(self) -> Dict[str, Any]:
        """Return Return the identification of the change of the data or in the cache for each source."""

        result = {}
        etag = hashlib.sha1()  # nosec
        for source in self._sources:
            if isinstance(source, SourceCache):
                result[source.value] = source.etag
                etag.update(source.etag.encode("utf-8"))
        result["etag"] = f'W/"{etag.hexdigest()}"'

        return wrap_dict_data(result)

    @auto_dbus  # type: ignore
    @property
    def extensions(self) -> List[str]:
        return self._sources.LOCAL.extensions

    @auto_dbus  # type: ignore
    @property
    def extensions_deprecated(self) -> List[str]:
        return self._sources.LOCAL.extensions_deprecated

    @auto_dbus  # type: ignore
    @property
    def has_deprecated(self) -> Dict[str, bool]:
        result = {}
        for source in self._sources:
            if source.name != self._sources.USB.name:
                result[source.value] = source.has_deprecated
        return wrap_dict_data(result)

    @auto_dbus
    @last_error
    def remove_all_deprecated_files(self) -> None:
        for source in self._sources:
            if source.name != self._sources.USB.name:
                source.remove_deprecated()

    @auto_dbus
    @last_error
    def move_deprecated_files(self) -> None:
        if not os.path.isdir(defines.OLD_PROJECTS_DOWNLOAD):
            os.makedirs(defines.OLD_PROJECTS_DOWNLOAD, exist_ok=True)
        for source in self._sources:
            if source.name != self._sources.USB.name:
                source.move_deprecated()
        self.PropertiesChanged(
            self.__INTERFACE__,
            {"len_download_deprecated_files": self.len_download_deprecated_files},
            [],
        )

    @auto_dbus
    @last_error
    def remove_downloaded_deprecated_files(self) -> None:
        for root, dirs, files in os.walk(defines.OLD_PROJECTS_DOWNLOAD, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        self.PropertiesChanged(
            self.__INTERFACE__,
            {"len_download_deprecated_files": self.len_download_deprecated_files},
            [],
        )

    @auto_dbus  # type: ignore
    @property
    def len_download_deprecated_files(self) -> int:
        try:
            return len(os.listdir(defines.OLD_PROJECTS_DOWNLOAD))
        except FileNotFoundError:
            return 0

    @auto_dbus
    def average_extraction_time(self, path: str) -> List[str]:
        return self._sources.average_extraction_time(path)
