# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

__version__ = "0.1.1"
__date__ = "23 Feb 2021"

__copyright__ = "(c) 2020 Prusa 3D"
__author_name__ = "Bruno Carvalho"
__author_email__ = "bruno.carvalho@prusa3d.cz"
__author__ = f"{__author_name__} <{__author_email__}>"
__description__ = "Prusa SLA File Manager"

__credits__ = "Bruno Carvalho, ..."
__url__ = "https://gitlab.com/prusa3d/sl1/filemanager.git"
