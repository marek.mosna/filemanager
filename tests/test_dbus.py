# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=import-error
# pylint: disable=no-self-use
# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import shutil

import pytest
import pydbus

from filemanager.sources import Sources


@pytest.mark.usefixtures("dbus")
@pytest.mark.usefixtures("clean")
class TestDbus:
    def test_last_update(self, wait_until):

        filemanager0 = pydbus.SystemBus().get("cz.prusa3d.sl1.filemanager0")

        with Sources() as sources:
            last_update = filemanager0.last_update[sources.LOCAL.value]
            new_project = sources.LOCAL.root / ("test1" + sources.extension)
            project_file = next(sources.LOCAL.root.glob("**/*" + sources.extension))

            shutil.copyfile(project_file, new_project)

            wait_until(
                lambda: filemanager0.last_update[sources.LOCAL.value] != last_update,
                timeout=1,
            )
            assert filemanager0.last_update[sources.LOCAL.value] != last_update
