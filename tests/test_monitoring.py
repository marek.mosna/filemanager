# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=import-error
# pylint: disable=no-self-use
# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import shutil
import logging

import pytest

from filemanager.sources import Sources
from filemanager.monitoring import ProjectsEvents


@pytest.mark.usefixtures("clean")
class TestMonitoring:
    def test_create(self, caplog, wait_until):
        caplog.set_level(logging.DEBUG)

        with Sources() as sources:

            last_update = sources.LOCAL.last_update
            new_project = sources.LOCAL.root / ("test1" + sources.extension)
            with ProjectsEvents(sources):

                # IN_CREATE (CREATE)
                project = next(sources.LOCAL.root.glob("**/*" + sources.extension))
                shutil.copyfile(project, new_project)
                wait_until(lambda: last_update != sources.LOCAL.last_update, timeout=1)
                assert last_update < sources.LOCAL.last_update
                assert "LOCAL FILE_CREATED" in caplog.text

    def test_move(self, caplog, wait_until):
        caplog.set_level(logging.DEBUG)

        with Sources() as sources:

            new_project = sources.LOCAL.root / ("test1" + sources.extension)
            project_file = next(sources.LOCAL.root.glob("**/*" + sources.extension))
            shutil.copyfile(project_file, new_project)
            caplog.clear()

            with ProjectsEvents(sources):

                # IN_MOVED_FROM (DELETE) => IN_MOVED_TO (CREATE)
                new_project.rename(sources.LOCAL.root / ("test2" + sources.extension))
                wait_until(
                    lambda: len(
                        [mgs for mgs in caplog.messages if "LOCAL FILE_CREATED" in mgs]
                    )
                    > 0,
                    timeout=2,
                )
                logs = caplog.messages
                assert len([mgs for mgs in logs if "LOCAL FILE_DELETED" in mgs]) > 0
                assert len([mgs for mgs in logs if "LOCAL FILE_CREATED" in mgs]) > 0

    # modify happens too much (ex.: before and after delete a file),
    # it might be better keep only CREATED/DELETED events

    # def test_modify(self, caplog, wait_until):
    #     caplog.set_level(logging.DEBUG)

    #     with Sources() as sources:

    #         new_project = sources.LOCAL.root / ("test1" + sources.extension)
    #         project_file = next(sources.LOCAL.root.glob("**/*" + sources.extension))
    #         shutil.copyfile(project_file, new_project)
    #         caplog.clear()

    #         with ProjectsEvents(sources):

    #             # MODIFY
    #             with zipfile.ZipFile(new_project, "a") as zip_file:
    #                 zip_file.writestr("test.txt", "increase size")
    #             wait_until(
    #                 lambda: len(
    #                     [mgs for mgs in caplog.messages if "LOCAL FILE_CHANGED" in mgs]
    #                 )
    #                 > 0,
    #                 timeout=2,
    #             )
    #             logs = caplog.messages
    #             assert len([mgs for mgs in logs if "LOCAL FILE_CHANGED" in mgs]) > 0

    def test_delete(self, caplog, wait_until):
        caplog.set_level(logging.DEBUG)

        with Sources() as sources:

            new_project = sources.LOCAL.root / ("test1" + sources.extension)
            project_file = next(sources.LOCAL.root.glob("**/*" + sources.extension))
            shutil.copyfile(project_file, new_project)
            wait_until(lambda: caplog.text, timeout=1)
            caplog.clear()

            with ProjectsEvents(sources):

                # DELETE
                new_project.unlink()
                wait_until(lambda: len(caplog.messages) > 7, timeout=1)
                logs = caplog.messages
                assert len([mgs for mgs in logs if "LOCAL FILE_DELETED" in mgs]) > 0
