# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=import-error
# pylint: disable=no-self-use
# pylint: disable=protected-access
# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import os
from pathlib import Path
import tempfile
import pytest

from filemanager import defines
from filemanager.sources import Sources
from filemanager.model import PrinterModel


class TestSource:
    @pytest.mark.usefixtures("clean")
    def test_root(self, monkeypatch):
        monkeypatch.setattr(Path, "is_mount", lambda x: True)

        with Sources() as sources:
            usb = Path(defines.PROJECT_MEDIA_PATH) / "sdb"
            usb.mkdir()
            assert sources.LOCAL.root == Path(defines.PROJECT_ROOT_PATH)
            assert sources.USB.root == usb

    def test_clean_cache(self, monkeypatch):
        # max size ~ 5MB
        # each files 2MB, so 4 files == 8MB, remove oldest folder (4MB)

        class Stat:
            def __init__(self, stat):
                self._stat = stat

            def __getattribute__(self, name):

                if name == "st_size":
                    return 2 * 1024 * 1024

                if name == "_stat":
                    return super().__getattribute__(name)

                return self._stat.__getattribute__(name)

        def get_stat(self):
            return Stat(os.stat(self))

        with Sources() as sources:
            origin = sources.LOCAL
            projects = origin.root.glob("**/*" + sources.extension)
            project_one = next(projects)
            project_two = next(projects)

            origin.metadata_thumbnail(project_one)
            with monkeypatch.context() as monk:
                monk.setattr(Path, "stat", get_stat)
                origin.metadata_thumbnail(project_two)
                origin._executor.submit(origin._clean_dir_cache)
                origin._executor.shutdown()
                assert origin.get_cache_item(project_one) == {}

    def test_printer_model(self):
        with tempfile.TemporaryDirectory() as model_path:
            defines.PRINTER_MODEL_PATH = Path(model_path)
            for model in PrinterModel:
                model_file = defines.PRINTER_MODEL_PATH / model.name.lower()
                if model is not PrinterModel.NONE:
                    model_file.touch()
                detected_model = PrinterModel.get_model()
                assert detected_model == model
                if model is not PrinterModel.NONE:
                    model_file.unlink()
