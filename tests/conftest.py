# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=import-error
# pylint: disable=no-self-use
# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
# pylint: disable=missing-class-docstring

import os
import time
from pathlib import Path
from threading import Thread

import pytest
import pydbus
from gi.repository import GLib

from filemanager import defines
from filemanager.dbus import FileManager0


@pytest.fixture(autouse=True)
def session_mock(monkeypatch, tmp_path):
    """Mock the dbus and examples path"""
    monkeypatch.setattr(pydbus, "SystemBus", pydbus.SessionBus)
    monkeypatch.setattr(defines, "PROJECT_ROOT_PATH", os.environ["EXAMPLES_PROJECTS"])
    monkeypatch.setattr(defines, "PROJECT_MEDIA_PATH", os.environ["EXAMPLES_MEDIA"])
    monkeypatch.setattr(defines, "PROJECT_PREVIOUS_PATH", tmp_path)
    monkeypatch.setattr(defines, "UPDATE_MEDIA_PATH", tmp_path)
    monkeypatch.setattr(defines, "PRINTER_MODEL_PATH", os.environ["PRINTER_MODEL_PATH"])


@pytest.fixture
def wait_until():
    """
    Wait until condition() is True
    """

    def _wait_until(condition, interval=0.1, timeout=1):
        start = time.time()
        while not condition() and time.time() - start < timeout:
            time.sleep(interval)

    return _wait_until


@pytest.fixture
def clean():
    """
    Delete files at the end of the test
    """
    yield
    local_root = Path(defines.PROJECT_ROOT_PATH)
    for path in local_root.glob("test*"):
        if path.is_file():
            path.unlink()
    usb = Path(defines.PROJECT_MEDIA_PATH) / "sdb"
    if usb.is_dir():
        usb.rmdir()


@pytest.fixture
def dbus():
    """
    Dbus interface
    """
    # monkeypatch.setattr(pydbus, "SystemBus", pydbus.SessionBus)
    loop = GLib.MainLoop()
    file_manager = FileManager0()
    pydbus.SystemBus().publish(FileManager0.__INTERFACE__, file_manager)
    loop_thread = Thread(target=loop.run, daemon=True)
    loop_thread.start()
    yield
    loop.quit()
    file_manager.stop()
    loop_thread.join()
